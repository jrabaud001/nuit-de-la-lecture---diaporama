---
author: Julien Rabaud
title: Nuit de la lecture - Diaporama
date: 2019-11-25

---

<script>
    Reveal.initialize({
        controls: false,
        controlsTutorial: false,
        controlsLayout: 'bottom-right',
        controlsBackArrows: 'faded',
        progress: false,
        slideNumber: false,
        hash: false,
        history: false,
        keyboard: true,
        overview: true,
        center: true,
        touch: true,
        loop: true,
        rtl: false,
        navigationMode: 'default',
        shuffle: true,
        fragments: true,
        fragmentInURL: false,
        embedded: false,
        help: true,
        showNotes: false,
        autoPlayMedia: null,
        preloadIframes: null,
        autoSlide: 4000,
        autoSlideStoppable: true,
        autoSlideMethod: Reveal.navigateNext,
        defaultTiming: 120,
        mouseWheel: false,
        hideInactiveCursor: true,
        hideCursorTime: 3000,
        hideAddressBar: true,
        previewLinks: false,
        transition: 'slide',
        transitionSpeed: 'medium',
        backgroundTransition: 'convex',
        viewDistance: 3,
        parallaxBackgroundImage: 'https://live.staticflickr.com/1307/5187954303_bbf1157200_k.jpg', 
        parallaxBackgroundSize: '1600px 660px',
        parallaxBackgroundHorizontal: 200,
        parallaxBackgroundVertical: 50,
        display: 'block'
    });
</script>
<style>
    .titre-photo {
        text-align: center;
        font-size: 14px;
        font-family: 'Garamond';
        color: white;
        
    }
    .fonds{
        background-color: rgba(150, 100, 30, .6);
        padding: 5px 7px;
        border-radius: 7px;
    }
    img {
        height: 550px;
        border: 5px solid rgba(150, 100, 30, .6);
    }
</style>

<div class="titre-photo" style="font-size: 22px; background-color: rgba(120, 65, 30, .5);">
\
    
Photographies sous licences *Creative Commons*

§§§

Utilisateurs *Flickr*

§§§

Keyworks : *book*, *lecture*, *livre*, *lecteurs*, *readings*...

§§§
    
\
</span></div>
    
    
## 
<img src="https://live.staticflickr.com/1967/45013555351_59667d010b_k.jpg" alt="Linear library - Vincent Aguerre"/>
<div class="titre-photo"><span class="fonds">Linear library - Vincent Aguerre</span></div>

## 
<img src="https://live.staticflickr.com/1826/29343643208_8926a2f122_k.jpg" title="DSC06277-05 - Suzy Hazelwood"/>
<div class="titre-photo"><span class="fonds">DSC06277-05 - Suzy Hazelwood</span></div>
    
## 
<img src="https://live.staticflickr.com/4499/37886150312_27f751c2dc_k.jpg" alt="Untitled - Santiago Sito" title="Untitled - Santiago Sito"/>
<div class="titre-photo"><span class="fonds">Untitled - Santiago Sito</span></div>

## 
<img src="https://live.staticflickr.com/866/39422168650_6f9a8f4879_k.jpg" alt="Sunlight On Pride And Prejudice - Suzy Hazelwood"/>
<div class="titre-photo"><span class="fonds">Sunlight On Pride And Prejudice - Suzy Hazelwood</span></div>

## 
<img src="https://live.staticflickr.com/7836/39787418493_8e2a82d38c_k.jpg" alt="Reading the newspaper - Pedro Ribeiro Simões"/>
<div class="titre-photo"><span class="fonds">Reading the newspaper - Pedro Ribeiro Simões</span></div>

## 
<img src="https://live.staticflickr.com/3277/2854731250_607d504191_k.jpg" alt="Surprise Transparent Landing - Wonderlane"/>
<div class="titre-photo"><span class="fonds">Surprise Transparent Landing - Wonderlane</span></div>

## 
<img src="https://live.staticflickr.com/8798/17887610862_e362f3d1b3_h.jpg" alt="Untitled - Miwok"/>
<div class="titre-photo"><span class="fonds">Untitled - Miwok</span></div>

## 
<img src="https://live.staticflickr.com/7393/9064318591_a9cd6ecebd_h.jpg" alt="Lecture - Lapichon"/>
<div class="titre-photo"><span class="fonds">Lecture - Lapichon</span></div>

## 
<img src="https://live.staticflickr.com/6010/5933740654_6a010a8f0b_h.jpg" alt="Deux appétits rassasiés - francoise_lesage"/>
<div class="titre-photo"><span class="fonds">Deux appétits rassasiés - francoise_lesage</span></div>

## 
<img src="https://live.staticflickr.com/4595/39518197282_b058b305b2_5k.jpg" alt="Capitol Hill Books - LWYang"/>
<div class="titre-photo"><span class="fonds">Capitol Hill Books - LWYang</span></div>

## 
<img src="https://live.staticflickr.com/6006/5965558512_084aec8a0b_k.jpg" alt="Livres - Shalott's Mirror"/>
<div class="titre-photo"><span class="fonds">Livres - Shalott's Mirror</span></div>

## 
<img src="https://live.staticflickr.com/2115/2262627348_4e1e564d89_h.jpg" alt="Renard lecteur 2 - Marc Beaudoin"/>
<div class="titre-photo"><span class="fonds">Renard lecteur 2 - Marc Beaudoin</span></div>

## 
<img src="https://live.staticflickr.com/1864/43855101184_70235a5cd7_h.jpg" alt="Words... - Sigma Ser"/>
<div class="titre-photo"><span class="fonds">Words... - Sigma Ser</span></div>

## 
<img src="https://live.staticflickr.com/7246/8167449320_ce6615ac85_h.jpg" alt="heart - Małgorzata K."/>
<div class="titre-photo"><span class="fonds">heart - Małgorzata K.</span></div>

## 
<img src="https://live.staticflickr.com/45/156567537_c02be4f60a_b.jpg" alt="library - Gunnar Tacke"/>
<div class="titre-photo"><span class="fonds">library - Gunnar Tacke</span></div>

## 
<img src="https://live.staticflickr.com/3749/9416465572_e06280c6a3_b.jpg" alt="n796_w1150 - Biodiversity Heritage Library"/>
<div class="titre-photo"><span class="fonds">n796_w1150 - Biodiversity Heritage Library</span></div>

## 
<img src="https://live.staticflickr.com/486/19709331440_1d8ec6335c_b.jpg" alt="concentration - Georgie Pauwels"/>
<div class="titre-photo"><span class="fonds">concentration - Georgie Pauwels</span></div>

## 
<img src="https://live.staticflickr.com/3086/3137926953_1ec3501619_c.jpg" alt="Books are for Reading - Andrew Hefter"/>
<div class="titre-photo"><span class="fonds">Books are for Reading - Andrew Hefter</span></div>

## 
<img src="https://live.staticflickr.com/2265/2072134438_bb943459a8_c.jpg" alt="love read - Luis de Bethencourt"/>
<div class="titre-photo"><span class="fonds">love read - Luis de Bethencourt</span></div>

## 
<img src="https://live.staticflickr.com/8706/16728170189_01edfd7f5f_c.jpg" alt="Sunday Newspaper - Georgie Pauwels"/>
<div class="titre-photo"><span class="fonds">Sunday Newspaper - Georgie Pauwels</span></div>

## 
<img src="https://live.staticflickr.com/4001/4685351483_253d4a2dbf_c.jpg" alt="Reading - Cushing Memorial Library and Archives, Texas A&M"/>
<div class="titre-photo"><span class="fonds">Reading - Cushing Memorial Library and Archives, Texas A&M</span></div>

## 
<img src="https://live.staticflickr.com/3447/3253265421_5fd3bfb573_c.jpg" alt="Woman reading, 2003 - Seattle Municipal Archives"/>
<div class="titre-photo"><span class="fonds">Woman reading, 2003 - Seattle Municipal Archives</span></div>

## 
<img src="https://live.staticflickr.com/7395/11968796204_942f2f6367_c.jpg" alt=". - Chiara Baldassarri"/>
<div class="titre-photo"><span class="fonds">. - Chiara Baldassarri</span></div>

## 
<img src="https://live.staticflickr.com/2660/4192987881_385d2ec713_c.jpg" alt="Arthur Conan Doyle room, Toronto Reference Library (Special Collections) - Special Collections Toronto Public Library"/>
<div class="titre-photo"><span class="fonds">Arthur Conan Doyle room, Toronto Reference Library (Special Collections) - Special Collections Toronto Public Library</span></div>

## 
<img src="https://live.staticflickr.com/87/217373929_7a66c381ab_c.jpg" alt="Biblioteca - rageforst æsthir"/>
<div class="titre-photo"><span class="fonds">Biblioteca - rageforst æsthir</span></div>

## 
<img src="https://live.staticflickr.com/1266/966213843_0c71fe0372_c.jpg" alt="Books - Nick"/>
<div class="titre-photo"><span class="fonds">Books - Nick</span></div>

## 
<img src="https://live.staticflickr.com/65535/48878273797_aeafe6f534_k.jpg" alt="Around Bologna - Slices of Light"/>
<div class="titre-photo"><span class="fonds">Around Bologna ... - Slices of Light</span></div>

## 
<img src="https://live.staticflickr.com/4807/44465117750_17f799b9f2_c.jpg" alt="Photo Series: It's a small macro world: 'Come again fro Canterbury' - Ken Whytock"/>
<div class="titre-photo"><span class="fonds">Photo Series: It's a small macro world: "Come again fro Canterbury" - Ken Whytock</span></div>

## 
<img src="https://live.staticflickr.com/4907/32844029368_5690270c3b_c.jpg" alt="10th January 2019 - themostinept"/>
<div class="titre-photo"><span class="fonds">10th January 2019 - themostinept</span></div>

## 
<img src="https://live.staticflickr.com/4697/39889902532_8ace5f6fb4_c.jpg" alt="Fantasy Book - Theo Crazzolara"/>
<div class="titre-photo"><span class="fonds">Fantasy Book - Theo Crazzolara</span></div>

## 
<img src="https://live.staticflickr.com/8757/28151699501_053b76a8c0_c.jpg" alt="أنجزت الكثير فيما يبدو فيما يتعلق بالقراءة لكن المزيد في الطريق- selfthy"/>
<div class="titre-photo"><span class="fonds">أنجزت الكثير فيما يبدو فيما يتعلق بالقراءة لكن المزيد في الطريق - selfthy</span></div>

## 
<img src="https://live.staticflickr.com/8710/17208042296_2c92fb3362_c.jpg" alt="Google without the colors - Vanie Castro"/>
<div class="titre-photo"><span class="fonds">Google without the colors - Vanie Castro</span></div>

## 
<img src="https://live.staticflickr.com/65535/48954713092_cc7a0bfb31_c.jpg" alt="21st October 2019 - themostinept"/>
<div class="titre-photo"><span class="fonds">21st October 2019 - themostinept</span></div>

## 
<img src="https://live.staticflickr.com/65535/48713656466_85ed1ce1ff_c.jpg" alt="Book with a razor blade (2003) - Chéma Madoz (1958) - Pedro Ribeiro Simões"/>
<div class="titre-photo"><span class="fonds">Book with a razor blade (2003) - Chéma Madoz (1958) - Pedro Ribeiro Simões</span></div>

## 
<img src="https://live.staticflickr.com/65535/48770226348_083ac4b884_c.jpg" alt="Madrid: Cuesta de Moyano - Jorge Franganillo"/>
<div class="titre-photo"><span class="fonds">Madrid: Cuesta de Moyano - Jorge Franganillo</span></div>

## 
<img src="https://live.staticflickr.com/159/437800417_03f76f62f1_c.jpg" alt="Mandragoras - sp!ros"/>
<div class="titre-photo"><span class="fonds">"Mandragoras" - sp!ros</span></div>

## 
<img src="https://live.staticflickr.com/3670/13538493323_136ab454a9_c.jpg" alt="Histoire abrégée de laville de Bergen-op-Zoom [Page titre] - BiblioArchives / LibraryArchives"/>
<div class="titre-photo"><span class="fonds">Histoire abrégée de la ville de Bergen-op-Zoom [Page titre] - BiblioArchives / LibraryArchives</span></div>

## 
<img src="https://live.staticflickr.com/2601/3707195746_32629db680_c.jpg" alt="The book bin - allispossible.org.uk"/>
<div class="titre-photo"><span class="fonds">The book bin - allispossible.org.uk</span></div>

## 
<img src="https://live.staticflickr.com/3928/15197798540_a3de806e53_c.jpg" alt="Lego Librarian rare books - M Cheung"/>
<div class="titre-photo"><span class="fonds">Lego Librarian rare books - M Cheung</span></div>

## 
<img src="https://live.staticflickr.com/499/18869291826_fa1d2e498d_c.jpg" alt="Rare Book - wiredforlego"/>
<div class="titre-photo"><span class="fonds">Rare Book - wiredforlego</span></div>

## 
<img src="https://live.staticflickr.com/2700/4286475665_85d98cafa2_c.jpg" alt="Yale University's Beinecke Rare Book and Manuscript Library - Lauren Manning"/>
<div class="titre-photo"><span class="fonds">Yale University's Beinecke Rare Book and Manuscript Library - Lauren Manning</span></div>

## 
<img src="https://live.staticflickr.com/2849/9038094822_2186a0d1ee_c.jpg" alt="Atlas coelestis / by the late Reverend Mr. John Flamsteed - State Library Victoria Collections"/>
<div class="titre-photo"><span class="fonds">Atlas coelestis / by the late Reverend Mr. John Flamsteed - State Library Victoria Collections</span></div>

## 
<img src="https://live.staticflickr.com/7784/17327805668_996f4c3e1e_c.jpg" alt="Lectrice (Bhaktapur, Népal) - Jean-François Gornet"/>
<div class="titre-photo"><span class="fonds">Lectrice (Bhaktapur, Népal) - Jean-François Gornet</span></div>

## 
<img src="https://live.staticflickr.com/5544/11073690984_bc8c52d893_c.jpg" alt="Reflet de lectrice. - Furcifer pardalis"/>
<div class="titre-photo"><span class="fonds">Reflet de lectrice. - Furcifer pardalis</span></div>

## 
<img src="https://live.staticflickr.com/2621/4075973018_ab074e3fcd_c.jpg" alt="Lille, 4 novembre 2009, à la bibliothèque - Kat..."/>
<div class="titre-photo"><span class="fonds">Lille, 4 novembre 2009, à la bibliothèque - Kat...</span></div>

## 
<img src="https://live.staticflickr.com/8451/8032517926_e85259d697_c.jpg" alt="La Lectrice - Sylv"/>
<div class="titre-photo"><span class="fonds">La Lectrice - Sylv</span></div>

## 
<img src="https://live.staticflickr.com/796/39109904660_50e9b2947b_c.jpg" alt="DSC06169-004t - Suzy Hazelwood"/>
<div class="titre-photo"><span class="fonds">DSC06169-004t - Suzy Hazelwood</span></div>

## 
<img src="https://live.staticflickr.com/4688/38431149565_3dc723e574_c.jpg" alt="Old Bookshop - Edinburgh - \@icandidyou"/>
<div class="titre-photo"><span class="fonds">Old Bookshop - Edinburgh - \@icandidyou</span></div>

## 
<img src="https://live.staticflickr.com/4388/36467362683_82bf527e21_c.jpg" alt="Thumbs up to a coffee break... - Julie anne Johnson"/>
<div class="titre-photo"><span class="fonds">Thumbs up to a coffee break... - Julie anne Johnson</span></div>

## 
<img src="https://live.staticflickr.com/4077/4849232643_a3f49d26ba_c.jpg" alt="Cambridge - Harvard Square: Harvard University - John Harvard - Wally Gobetz"/>
<div class="titre-photo"><span class="fonds">Cambridge - Harvard Square: Harvard University - John Harvard - Wally Gobetz</span></div>

## 
<img src="https://live.staticflickr.com/5653/21116046615_a0e1598a7e_c.jpg" alt="le lecteur - Bernard Fidel"/>
<div class="titre-photo"><span class="fonds">le lecteur - Bernard Fidel</span></div>

## 
<img src="https://live.staticflickr.com/5234/5898299017_b872365061_z.jpg" alt="le lecteur - Jean Boechat"/>
<div class="titre-photo"><span class="fonds">le lecteur - Jean Boechat</span></div>

## 
<img src="https://live.staticflickr.com/3806/13443973654_7b9eeb090f_z.jpg" alt="Lettres - Jose"/>
<div class="titre-photo"><span class="fonds">Lettres - Jose</span></div>

## 
<img src="https://live.staticflickr.com/3076/2633641873_28f8cc7dd3_z.jpg" alt="lettres... - kzper"/>
<div class="titre-photo"><span class="fonds">lettres... - kzper</span></div>

## 
<img src="https://live.staticflickr.com/3598/3363630437_73dd3351d0_z.jpg" alt="lettre - Huonganh"/>
<div class="titre-photo"><span class="fonds">lettre - Huonganh</span></div>

## 
<img src="https://live.staticflickr.com/8152/6969289738_8e2ef4106d_z.jpg" alt="dutracé p0 - patricia m"/>
<div class="titre-photo"><span class="fonds">dutracé p0 - patricia m</span></div>

## 
<img src="https://live.staticflickr.com/8641/15633285744_716cbaf37f_c.jpg" alt="Read - Jodie Dee"/>
<div class="titre-photo"><span class="fonds">Read - Jodie Dee</span></div>

## 
<img src="https://live.staticflickr.com/5831/23139539570_b62fab3b43_c.jpg" alt="Read - eduardo gomez"/>
<div class="titre-photo"><span class="fonds">Read - eduardo gomez</span></div>

## 
<img src="https://live.staticflickr.com/4864/31007086377_e6a51f7c02.jpg" alt="Read - squarequilter (Betty)"/>
<div class="titre-photo"><span class="fonds">Read - squarequilter (Betty)</span></div>

## 
<img src="https://live.staticflickr.com/4604/40176248632_7bd98d1e2f_c.jpg" alt="The Beauty of Reading - Jodie Dee"/>
<div class="titre-photo"><span class="fonds">The Beauty of Reading - Jodie Dee</span></div>

## 
<img src="https://live.staticflickr.com/22/30441950_9d98777b29_c.jpg" alt="Yellow-Pages - Emiliano Pennisi"/>
<div class="titre-photo"><span class="fonds">Yellow-Pages - Emiliano Pennisi</span></div>

## 
<img src="https://live.staticflickr.com/2325/2093762352_5406e9b2bb_c.jpg" alt="page - gamebouille"/>
<div class="titre-photo"><span class="fonds"></span>page - gamebouille</div>

## 
<img src="https://live.staticflickr.com/65535/47950645076_fd57ee9a57.jpg" alt="Enfant découvrant le plaisir de lire - Jacques Chiesa"/>
<div class="titre-photo"><span class="fonds">Enfant découvrant le plaisir de lire - Jacques Chiesa</span></div>

## 
<img src="https://live.staticflickr.com/5457/30619024480_0aa65da1af.jpg" alt="Lire... quand les livres s'effacent - p.guayacan"/>
<div class="titre-photo"><span class="fonds">Lire... quand les livres s'effacent - p.guayacan</span></div>

## 
<img src="https://live.staticflickr.com/8444/29400872655_feb93498d7.jpg" alt="Lire - Maxence"/>
<div class="titre-photo"><span class="fonds">Lire - Maxence</span></div>

## 
<img src="https://live.staticflickr.com/4454/37684618522_deb55ddb4e.jpg" alt="Flower story - Carandoom"/>
<div class="titre-photo"><span class="fonds">Flower story - Carandoom</span></div>

## 
<img src="https://live.staticflickr.com/4848/46962272132_afe79a4312.jpg" alt="Tranh dân gian Đông Hồ đầu thế kỷ 20: Thầy đồ cóc, hay Ếch đi học - manhhai"/>
<div class="titre-photo"><span class="fonds">Tranh dân gian Đông Hồ đầu thế kỷ 20: Thầy đồ cóc, hay Ếch đi học - manhhai</span></div>

## 
<img src="https://live.staticflickr.com/5588/14844502554_8dbf1580b4.jpg" alt="My laptop - Christian Jann"/>
<div class="titre-photo"><span class="fonds">My laptop - Christian Jann</span></div>

## 
<img src="https://live.staticflickr.com/946/26984486797_d7f0046bb7.jpg" alt="Written in the Margins 41 - VCU Libraries"/>
<div class="titre-photo"><span class="fonds">Written in the Margins 41 - VCU Libraries</span></div>

## 
<img src="https://live.staticflickr.com/3688/11350849205_2221275d6c.jpg" alt="Les magasins - Bibliothèque des Champs Libres"/>
<div class="titre-photo"><span class="fonds">Les magasins - Bibliothèque des Champs Libres</span></div>

## 
<img src="https://live.staticflickr.com/2093/2435823037_7853d39e69_z.jpg" alt="labyrinthine circuit board lines - quapan"/>
<div class="titre-photo"><span class="fonds">labyrinthine circuit board lines - quapan</span></div>

## 
<img src="https://live.staticflickr.com/6150/5926270312_68d81394eb_4k.jpg" alt="Reading - Kevin Dooley"/>
<div class="titre-photo"><span class="fonds">Reading - Kevin Dooley</span></div>

## 
<img src="https://live.staticflickr.com/1950/30213988667_1d86b4fd06.jpg" alt="Books - Vicente"/>
<div class="titre-photo"><span class="fonds">Books - Vicente</span></div>

## 
<img src="https://live.staticflickr.com/7139/7825208498_484216ee94.jpg" alt="Libres! - alpuerto"/>
<div class="titre-photo"><span class="fonds">Libres! - alpuerto</span></div>

## 
<img src="https://live.staticflickr.com/4891/31825028138_bbc7624224.jpg" alt="Chained Library Tour, Wells Cathedral - T_Marjorie"/>
<div class="titre-photo"><span class="fonds">Chained Library Tour, Wells Cathedral - T_Marjorie</span></div>

## 
<img src="https://live.staticflickr.com/6180/6187758941_4eff92bd0b.jpg" alt="Holy book - Pedro Ribeiro Simões"/>
<div class="titre-photo"><span class="fonds">Holy book - Pedro Ribeiro Simões</span></div>

## 
<img src="https://live.staticflickr.com/4480/26250915699_e935b6a0ea.jpg" alt="Vinyl - Pete"/>
<div class="titre-photo"><span class="fonds">Vinyl - Pete</span></div>

## 
<img src="https://live.staticflickr.com/711/22648805163_817b49fe30.jpg" alt="Je Sais Tout 15 Jun 1905 - stekelbes"/>
<div class="titre-photo"><span class="fonds">Je Sais Tout 15 Jun 1905 - stekelbes</span></div>

## 
<img src="https://live.staticflickr.com/8502/8433916952_8a257f4f2b.jpg" alt="030-Joh. Michaelis Faustij ... Compendium alchymist….1706-Johann Michael Faust - Cesar Ojeda"/>
<div class="titre-photo"><span class="fonds">030-Joh. Michaelis Faustij ... Compendium alchymist….1706-Johann Michael Faust - Cesar Ojeda</span></div>
## 
<img src="https://live.staticflickr.com/5527/10158918723_ef5f157d5c.jpg" alt="We consider the Illinois Watch without exception the best in the world. [front] - Boston Public Library"/>
<div class="titre-photo"><span class="fonds">We consider the Illinois Watch without exception the best in the world. [front] - Boston Public Library</span></div>

## 
<img src="https://live.staticflickr.com/7511/15674521773_734ae52876.jpg" alt="Cartagena - Colombia - Alexander Schimmeck"/>
<div class="titre-photo"><span class="fonds">Cartagena - Colombia - Alexander Schimmeck</span></div>

## 
<img src="https://live.staticflickr.com/7378/8728631592_63724d4897.jpg" alt="Louna au Musée - Helena Perez García"/>
<div class="titre-photo"><span class="fonds">Louna au Musée - Helena Perez García</span></div>

## 
<img src="https://live.staticflickr.com/7327/8728631708_4eb8ae278f.jpg" alt="Louna au Musée [2] - Helena Perez García"/>
<div class="titre-photo"><span class="fonds">Louna au Musée [2] - Helena Perez García</span></div>

## 
<img src="https://live.staticflickr.com/65535/48798119007_c2a4d39110.jpg" alt="IMG_0608J Adriaen van Ostade. 1610-1685. Haarlem L'analyse. Analysis 1666 Paris Petit Palais (Musée des Beaux Arts de la Ville de Paris) - jean louis mazieres"/>
<div class="titre-photo"><span class="fonds">IMG_0608J Adriaen van Ostade. 1610-1685. Haarlem L'analyse. Analysis 1666 Paris Petit Palais (Musée des Beaux Arts de la Ville de Paris) - jean louis mazieres</span></div>

## 
<img src="https://live.staticflickr.com/938/29860662788_b1714c7b47.jpg" alt="Animal Atlas - Brickset"/>
<div class="titre-photo"><span class="fonds">Animal Atlas - Brickset</span></div>

## 
<img src="https://live.staticflickr.com/6156/6165222486_0f1335f031.jpg" alt="L'art de lire, c'est l'art de penser avec un peu d'aide. - Martine"/>
<div class="titre-photo"><span class="fonds">L'art de lire, c'est l'art de penser avec un peu d'aide. - Martine</span></div>

## 
<img src="https://live.staticflickr.com/8275/30429201415_f977afc051.jpg" alt="La Camaraderie, Rébus, 2015 - Retis"/>
<div class="titre-photo"><span class="fonds">La Camaraderie, Rébus, 2015 - Retis</span></div>

## 
<img src="https://live.staticflickr.com/4057/5139175030_1a19926907.jpg" alt="La lecture. IMG_3680 - bookchen"/>
<div class="titre-photo"><span class="fonds">La lecture. IMG_3680 - bookchen</span></div>

## 
<img src="https://live.staticflickr.com/8588/15944800683_880eaa24af.jpg" alt="La bibliothèque d'Alexandrie (Egypte) - Jean-Pierre Dalbéra"/>
<div class="titre-photo"><span class="fonds">La bibliothèque d'Alexandrie (Egypte) - Jean-Pierre Dalbéra</span></div>

## 
<img src="https://live.staticflickr.com/5091/5405518704_2bca6703a1.jpg" alt="DSC_6174 - detail - Salon de Lecture - Corinne Moncelli"/>
<div class="titre-photo"><span class="fonds">DSC_6174 - detail - Salon de Lecture - Corinne Moncelli</span></div>

## 
<img src="https://live.staticflickr.com/3894/14847940090_eca7befdba.jpg" alt="Reading can be difficult - Alexandre Dulaunoy"/>
<div class="titre-photo"><span class="fonds">Reading can be difficult - Alexandre Dulaunoy</span></div>

## 
<img src="https://live.staticflickr.com/6152/6182566649_a515051e74.jpg" alt="Reading on the beach - Lecture sur la plage - Pol Skas"/>
<div class="titre-photo"><span class="fonds">Reading on the beach - Lecture sur la plage - Pol Skas</span></div>

## 
<img src="https://live.staticflickr.com/8091/8562082880_027acda043_z.jpg" alt="ETUDIANT - Gilles ESPIEUSSAS"/>
<div class="titre-photo"><span class="fonds">ETUDIANT - Gilles ESPIEUSSAS</span></div>

## 
<img src="https://live.staticflickr.com/5832/21092133405_d771febc3c_z.jpg" alt="Étudiant - Hadrien Hart"/>
<div class="titre-photo"><span class="fonds">Étudiant - Hadrien Hart</span></div>

## 
<img src="https://live.staticflickr.com/5605/30647302753_c324bc22a7_z.jpg" alt="Atelier participatif aux Transmusicales - tubeaidees asso"/>
<div class="titre-photo"><span class="fonds">Atelier participatif aux Transmusicales - tubeaidees asso</span></div>

## 
<img src="https://live.staticflickr.com/265/19150854871_6c77291a4e_z.jpg" alt="etudiant au travail - Campus France"/>
<div class="titre-photo"><span class="fonds">etudiant au travail - Campus France</span></div>

## 
<img src="https://live.staticflickr.com/5765/31418583026_4fa54af8e5_z.jpg" alt="Atelier participatif aux Transmusicales [2] - tubeaidees asso"/>
<div class="titre-photo"><span class="fonds">Atelier participatif aux Transmusicales [2] - tubeaidees asso</span></div>

## 
<img src="https://live.staticflickr.com/2872/11098109436_1a55c4eca3_z.jpg" alt="Etudiants d'archi - Jean-François Gornet"/>
<div class="titre-photo"><span class="fonds">Etudiants d'archi - Jean-François Gornet</span></div>

## 
<img src="https://live.staticflickr.com/65535/48886798396_d09aba1194_z.jpg" alt="201910_goupe_001_web - Frédéric Bach"/>
<div class="titre-photo"><span class="fonds">201910_goupe_001_web - Frédéric Bach</span></div>

## 
<img src="https://live.staticflickr.com/2061/2223004424_4b510ee206_z.jpg" alt="Home (romantic) strobism, 'L'étudiant' - Alessandro"/>
<div class="titre-photo"><span class="fonds">Home (romantic) strobism, "L'étudiant" - Alessandro</span></div>

## 
<img src="https://live.staticflickr.com/2820/9638591092_98bf4167c5_z.jpg" alt="travailler moins pour lire plus - ActuaLitté"/>
<div class="titre-photo"><span class="fonds">travailler moins pour lire plus - ActuaLitté</span></div>

## 
<img src="https://live.staticflickr.com/3735/9638585176_bbd0fb1a8b_z.jpg" alt="travailler moins pour lire plus [2] - ActuaLitté"/>
<div class="titre-photo"><span class="fonds">travailler moins pour lire plus [2] - ActuaLitté</span></div>

## 
<img src="https://live.staticflickr.com/2670/3937574362_66723a06cf_z.jpg" alt="travail - francois karm"/>
<div class="titre-photo"><span class="fonds">travail - francois karm</span></div>

## 
<img src="https://live.staticflickr.com/2846/8757770321_849714b831_z.jpg" alt="Just work like it - Julie Rieg"/>
<div class="titre-photo"><span class="fonds">Just work like it - Julie Rieg</span></div>

## 
<img src="https://live.staticflickr.com/6090/6026362847_05ee50e8f6_z.jpg" alt="Travailler- - djetro"/>
<div class="titre-photo"><span class="fonds">Travailler- - djetro</span></div>

## 
<img src="https://live.staticflickr.com/1153/990438762_461431962a_z.jpg" alt="au travail - bonsound"/>
<div class="titre-photo"><span class="fonds">au travail - bonsound</span></div>

## 
<img src="https://live.staticflickr.com/441/19434948371_e9df3ef47a_z.jpg" alt="Travail dans la cour - Jean-François Gornet"/>
<div class="titre-photo"><span class="fonds">Travail dans la cour - Jean-François Gornet</span></div>

## 
<img src="https://live.staticflickr.com/3587/3770011850_74afe2cf29_z.jpg" alt="Shards - Stinging Eyes"/>
<div class="titre-photo"><span class="fonds">Shards - Stinging Eyes</span></div>

## 
<img src="https://live.staticflickr.com/3274/3063220244_febaf28d96_z.jpg" alt="Una Code - Dino Latoga"/>
<div class="titre-photo"><span class="fonds">Una Code - Dino Latoga</span></div>

## 
<img src="https://live.staticflickr.com/7686/28461969021_9a5b9263eb_z.jpg" alt="Code red! - Blondinrikard Fröberg"/>
<div class="titre-photo"><span class="fonds">Code red! - Blondinrikard Fröberg</span></div>

## 
<img src="https://live.staticflickr.com/4812/31931534008_ec45719326_z.jpg" alt="Morse code - Eric.Ray"/>
<div class="titre-photo"><span class="fonds">Morse code - Eric.Ray</span></div>

## 
<img src="https://live.staticflickr.com/7391/10383620993_ffdaa750ca_z.jpg" alt="Code - Alina Sandu"/>
<div class="titre-photo"><span class="fonds">Code - Alina Sandu</span></div>

## 
<img src="" alt=""/>
<div class="titre-photo"><span class="fonds"></span></div>

<script>
    Reveal.addEventListener( 'slidechanged', function( event ) {
    if( Reveal.isFirstSlide() ) {
        // Randomize the order again
        Reveal.shuffle();

        // Navigate to the first slide according to the new order
        Reveal.slide( 0, 0 );
    }
} );
</script>
