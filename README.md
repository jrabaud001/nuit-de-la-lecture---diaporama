# Nuit de la lecture - diaporama

- Mettre dans un fichier markdown des `<img>` de flickr (creative commons) avec leur 'titre - nom d'utilisateur' dans l'attribut `alt=""`
- Collaborer
- Tester le [Gitlab de l'UPPA](https://git.univ-pau.fr/)
- commande pandoc :
```
pandoc -t revealjs --standalone --self-contained Nuit_de_la_lecture.md -o NuitdelaLecture.html --extract-media=data --resource-path=data

```